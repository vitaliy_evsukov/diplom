var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var config = require('../config/config');

require('../api/v1/models/user');
var users = mongoose.model('user');

exports.check = function(req, res, next){
    var token = req.headers['x-access-token'];
    if(token){
        jwt.verify(token, config.jwtKey, function(err,decoded){
            if(err){
                //TODO
                req.access = -1;
            } else {
                users.findById(decoded.id, function(err,user){
                    if(err || !user){
                        //TODO
                        req.access = -1;
                    } else {
                        req.access = user.access;
                    }
                });
            }
        });
    } else {
        req.access = 0;
    }
    next();
}
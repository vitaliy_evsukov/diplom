const express = require('express');
const parser = require('body-parser');
const mongoose = require('mongoose');
const app = express();

mongoose.Promise = global.Promise;
var mongoDB = 'mongodb://localhost';
mongoose.connect(mongoDB);

app.use(parser.urlencoded({extended: true}));
app.use(parser.json());

//Midlewарезник
var token= require('./middleware/token');
app.use(token.check);

//API
var user = require('./api/v1/routes/user');
app.use('/api/v1/', user);
var auth = require('./api/v1/routes/auth');
app.use('/api/v1/', auth);

app.listen(8080, function()
{
    console.log("WORKING");
});
 

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).json({code: 404, message: "Ресурса нету"});
   });

  
  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    code = err.status || 500;
    res.status(code).json({code: code, message: err.message});
  });
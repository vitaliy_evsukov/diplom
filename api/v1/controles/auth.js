const express = require('express');
const mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var config = require('../../../config/config');

require('../models/user');
var user = mongoose.model('user');

exports.auth = function(req,res){
    if(!req.body.email && !req.body.password){
        res.status(400).json({
            code:400,
            message:"GG WP"
        });
        return;
    }
    user.findOne({email:req.body.email}, function(err, user){
        if(err || !user){
            res.status(200).json({ auth: false, email:false, pass: false});
        }else {
            if(user.pass === req.body.pass){
                var token = jwt.sign({id: user._id}, config.jwtKey,{
                    expiresIn: 86400 * 7
                })
                res.status(200).json({
                    auth: true,
                    email: true,
                    pass: true,
                    token: token
                })
            } else {
                res.status(200).json({
                    auth: false,
                    email: true,
                    pass: false
                });
            }
        }
    });
}
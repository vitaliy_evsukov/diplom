const express = require('express');
const mongoose = require('mongoose');
require('../models/user');
const user = mongoose.model('user');



exports.all = function(req, res){
    user.find({},function(err, user){
        if(!err){
            res.status(200).json(user);
        }
        else{
            res.status(400).json({code:400,message: "Bad request"})
        }})
};

exports.new = function(req,res){
    var newUser = new user(req.body);

    newUser.save(function(err,user){
        if(err){
            res.json(err);
        }
        else{
            res.json(user);
            
        };
    })
};
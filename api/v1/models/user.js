const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String, 
        required: true,
        unique:true, 
    },
    pass: {
        type: String, 
        required: true, 
        
    },
    firstname: {
        type: String, 
        required: true
    },
    lastname: {
        type: String, 
        required: true, 
    },
    access:{
       type: Number,
       required: true,
       default: 0
    }

}) 

module.exports = mongoose.model('user', UserSchema);
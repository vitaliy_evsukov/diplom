const express = require('express');
var router = express.Router();
var auth = require('../controles/auth');

router.post('/auth', auth.auth);

module.exports = router;
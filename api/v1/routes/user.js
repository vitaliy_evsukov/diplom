const express = require('express');
var router = express.Router();
var users = require('../controles/user');

router.get('/user', users.all);
router.post('/user', users.new);

module.exports = router;